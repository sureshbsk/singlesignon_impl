package com.mycompany.practiceApp_SAML;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeAppSamlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeAppSamlApplication.class, args);
	}
}
